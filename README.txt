These scripts make bubble plots.

The bubble functions are in bubble_package.R

make_bubbles.R is a helper script that helps you call the bubbleplot() function

Parse_PANTHER_for_simREL.R is a set of scripts for preparing the necessary category files based on the PANTHER ontology. 
I used this for my urchin work. It may be more or less useful for other species.

Bubbles are parameterized by:
1) category name
2) bubble size
3) bubble color
4) bubble coordinates

make_bubbles.R can calculate the above parameters given:
1) a file like gene_to_cat_list.txt that lists each gene and all the GO terms that is is annotated with
	- this example file has GO terms in at least 3 categories (BP, CC, MF). You'll probably want to extract just one category per plot
2) Two vectors of categories to plot and corresponding scores	
3) a file that describes the distance between each category. This is a matrix with row and column names corresponding to the GO terms. 
	-This matrix can be created with the calcSimRel() function, or in the Parse_PANTHER_for_simREL.R script.
	-an example matrix is: BP_simRel_scores.dat
	-note that the make_bubbles.R script expects this file to be in the Semantic_Similarity_scores directory.


Some relatively easy improvements would be:
-use ggplot2 to draw the plot (this would automatically include the scale bars which I don't do well)
-use scales package to scale the circle sizes and colors	